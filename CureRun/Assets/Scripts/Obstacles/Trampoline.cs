﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : Obstacle {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Destroy ourself if we collide with a player
	protected override void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.layer == playerLayer) {
			Debug.Log("Collision with obstacle: " + this.ToString());
			// gotta somehow apply effect to the player
			collision.GetComponent<Player>().ApplyEffect("bounce", value);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart : MonoBehaviour {
    
    public Sprite fullHeart;
    public Sprite emptyHeart;
    public Player player;
    public int id;

    private Image image;

    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (id < player.GetHealth())
        {
            image.sprite = fullHeart;
        } else
        {
            image.sprite = emptyHeart;
        }
	}
}

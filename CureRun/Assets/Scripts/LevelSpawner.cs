﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour {

	[SerializeField]
	GroundChunk[] spawnableGroundChunks;
	[SerializeField]
	GroundChunk[] spawnableRoofChunks;
	List<GroundChunk> currentChunks = new List<GroundChunk>();
    [SerializeField]
	float chunkSpeed;
	float maxJumpWidth = 2f;
	float maxJumpHeight = 0.8f;

	[SerializeField]
	bool onRoof = false;
	const float widthPastEdge = 100;
	

	// Use this for initialization
	void Start () {
		Player currentPlayer = FindObjectOfType<Player>();
		maxJumpHeight = currentPlayer.maxJumpDuration * currentPlayer.jumpSpeed *0.9f ;
		maxJumpWidth = (currentPlayer.maxJumpDuration * chunkSpeed) + ((maxJumpHeight / currentPlayer.fallSpeed) * chunkSpeed);
	}

	void OnEnable(){
		//Place starting area chunks
		onRoof = false;
		float startChunkWidth = spawnableGroundChunks[0].transform.localScale.x;
		float startChunkHeight = spawnableGroundChunks[0].transform.localScale.y;
		for(float pos = -startChunkWidth*2; pos < startChunkWidth*4; pos+=startChunkWidth){
			GroundChunk startChunk = Instantiate(spawnableGroundChunks[0]);
			startChunk.transform.SetParent(transform,false);
			startChunk.transform.position = new  Vector3(pos,startChunk.transform.position.y,0);
			currentChunks.Add(startChunk);
		}
	}
	void OnDisable(){
		for(int i = currentChunks.Count-1; i>=0; i--){
			GroundChunk chunk = currentChunks[i];
			currentChunks.Remove(chunk);
			Destroy(chunk.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		for(int i =0; i<currentChunks.Count; i++){
			MoveChunk(currentChunks[i]);
		}
		SpriteRenderer lastChunk = currentChunks[currentChunks.Count-1].GetComponent<SpriteRenderer>();
		Vector3 lastChunkScreenPos = Camera.main.WorldToScreenPoint(lastChunk.transform.position - lastChunk.bounds.extents);
		if(lastChunkScreenPos.x < Screen.width + widthPastEdge){
			SpawnNewChunk();
		}
	}

	private void MoveChunk(GroundChunk chunk){
		chunk.transform.Translate(chunk.transform.right * -chunkSpeed * Time.deltaTime,Space.Self);
		Vector3 chunkScreenPos = Camera.main.WorldToScreenPoint(chunk.transform.position + chunk.transform.localScale/2f);
		if(chunkScreenPos.x < 0){
			currentChunks.Remove(chunk);
			Destroy(chunk.gameObject);
		}
	}

	private void SpawnNewChunk(){
		if(!onRoof){
			Vector3 lastChunkPos = currentChunks[currentChunks.Count-1].transform.position;

            int chunkIndex = Random.Range(0, spawnableGroundChunks.Length);

			float chunkWidth = spawnableGroundChunks[chunkIndex].transform.localScale.x;
			GroundChunk newChunk = Instantiate(spawnableGroundChunks[chunkIndex]);
			newChunk.transform.SetParent(transform,false);
			newChunk.transform.position = new  Vector3(lastChunkPos.x+chunkWidth,newChunk.transform.position.y,0);
			currentChunks.Add(newChunk);
		}
		else{
			Vector3 lastChunkPos = currentChunks[currentChunks.Count-1].transform.position;

            int chunkIndex = Random.Range(0, spawnableRoofChunks.Length);

            float chunkWidth = spawnableRoofChunks[chunkIndex].transform.localScale.x;
			GroundChunk newChunk = Instantiate(spawnableRoofChunks[chunkIndex]);
			newChunk.transform.SetParent(transform,false);

			float widthToHeight = Random.Range(0f,1f);
			float heightToWidth = 1-widthToHeight;
			Vector3 newChunkPos = new Vector3();
			newChunkPos.x = lastChunkPos.x + chunkWidth + Random.Range(0,maxJumpWidth*widthToHeight);
			newChunkPos.y = lastChunkPos.y + Random.Range(-maxJumpHeight*heightToWidth,maxJumpHeight*heightToWidth);
			newChunk.transform.position = newChunkPos;
			currentChunks.Add(newChunk);
		}
	}
}

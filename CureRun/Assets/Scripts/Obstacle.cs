﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Obstacle : MonoBehaviour {

	[SerializeField]
	protected int playerLayer;

	[SerializeField]
	protected int value = 1;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Destroy ourself if we collide with a player
	protected abstract void OnTriggerEnter2D(Collider2D collision);
}

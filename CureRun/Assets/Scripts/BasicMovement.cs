﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovement : MonoBehaviour {

	[SerializeField]
	protected float xSpeed = 0f;
	[SerializeField]
	protected float ySpeed = 0f;
	
	// Children that inherit from this class must initialize the state of this class
	void Start () {
		
	}

	// Children that inherit from this class must delete Update() or call base.Update() or call UpdatePosition();
	void Update () {
		UpdatePosition();
		SpriteRenderer sprite = GetComponent<SpriteRenderer>();
		if(sprite != null){
			Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position + sprite.bounds.extents);
			if(screenPos.x < -20){
				Destroy(gameObject);
			}
		}
	}
	protected void UpdatePosition() {
		this.transform.position += new Vector3(xSpeed * Time.deltaTime, ySpeed * Time.deltaTime, 0);
	}
}

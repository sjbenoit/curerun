﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyGradient : MonoBehaviour {

	[SerializeField]
	Gradient skyGradient;

	float currentTime = 0;
	[SerializeField]
	float timeRate = 0.0025f;

	Camera skyCam;

	[SerializeField]
	GameObject game; //Hacky way of seeing if game is running

	// Use this for initialization
	void Start () {
		skyCam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(game.activeSelf){
		currentTime = currentTime%1;
		skyCam.backgroundColor = skyGradient.Evaluate(currentTime);
		currentTime += Time.deltaTime * timeRate;
		}
		else{
			currentTime = 0;
		}
	}
}

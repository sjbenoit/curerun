﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	[SerializeField]
	protected int playerLayer;

	[SerializeField]
	protected int value = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	// Destroy ourself if we collide with a player
	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.layer == playerLayer) {
			Debug.Log("Collision with pickup: " + this.ToString());
            collision.GetComponent<Player>().ApplyPoints(this.value);
            Destroy(this.gameObject);
		}
	}
}

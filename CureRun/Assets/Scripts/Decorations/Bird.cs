﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : Flying {

	// Use this for initialization
	void Start () {
		xSpeed = -1;
		yEnabled = true;
		yDirection = true;
		ySpeedUp = 0.1f;
		ySpeedDown = 0.2f;
		yUpperBound = 1f;
	}

}

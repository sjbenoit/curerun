﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterStand : Obstacle {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected override void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.layer == playerLayer) {
			Debug.Log("Collision with obstacle: " + this.ToString());
			// gotta somehow apply damage to the player
			//collision.GetComponent<Player>().Hydrate(this.value);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpawner : MonoBehaviour {

	#region Member structs
	[System.Serializable]
	struct BackgroundRegion{
		public BackgroundChunk[] spawnableChunks;
	}
	#endregion

	[SerializeField]
	BackgroundRegion[] spawnableRegions;

	BackgroundRegion currentRegion;

	List<BackgroundChunk> currentChunks = new List<BackgroundChunk>();
	[SerializeField]
	private float chunkSpeed = 0.25f;
	[SerializeField]
	private int backgroundRegionLength = 10;
	private const float widthPastEdge = 100f;
	private const float buildingHeightOffset = 2;

	private int regionSpawns = 0;
	
	void OnEnable(){
		currentRegion = spawnableRegions[Random.Range(0,spawnableRegions.Length)];
		//Place starting area chunks
		int spawnChunk = Random.Range(0,currentRegion.spawnableChunks.Length);
		float startChunkWidth = currentRegion.spawnableChunks[spawnChunk].transform.localScale.x;
		for(float pos = -startChunkWidth * 2; pos < startChunkWidth * 2; pos += startChunkWidth){
			BackgroundChunk startChunk = Instantiate(currentRegion.spawnableChunks[spawnChunk]);
			startChunk.transform.SetParent(transform, false);
			startChunk.transform.position = new  Vector3(pos, startChunk.transform.position.y, 0);
			currentChunks.Add(startChunk);
			UnpackChunk(startChunk);
			spawnChunk = Random.Range(0,currentRegion.spawnableChunks.Length);
		}
	}
	void OnDisable(){
		for(int i = currentChunks.Count-1; i>=0; i--){
			BackgroundChunk chunk = currentChunks[i];
			currentChunks.Remove(chunk);
			Destroy(chunk.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		for(int i =0; i < currentChunks.Count; i++){
			MoveChunk(currentChunks[i]);
		}
		SpriteRenderer lastChunk = currentChunks[currentChunks.Count-1].GetComponent<SpriteRenderer>();
		Vector3 lastChunkScreenPos = Camera.main.WorldToScreenPoint(lastChunk.transform.position - lastChunk.bounds.extents);
		if(lastChunkScreenPos.x < Screen.width + widthPastEdge){
			SpawnNewChunk();
		}
	}

	private void MoveChunk(BackgroundChunk chunk){
		chunk.transform.Translate(chunk.transform.right * -chunkSpeed * Time.deltaTime, Space.Self);
		Vector3 chunkScreenPos = Camera.main.WorldToScreenPoint(chunk.transform.position + chunk.transform.localScale/2f);
		if(chunkScreenPos.x < 0 - widthPastEdge){
			currentChunks.Remove(chunk);
			Destroy(chunk.gameObject);
		}
	}

	private void SpawnNewChunk(){
		Vector3 lastChunkPos = currentChunks[currentChunks.Count - 1].transform.position;
		int spawnChunk = Random.Range(0,currentRegion.spawnableChunks.Length);
		float chunkWidth = currentRegion.spawnableChunks[spawnChunk].transform.localScale.x;
		BackgroundChunk newChunk = Instantiate(currentRegion.spawnableChunks[spawnChunk]);
		newChunk.transform.SetParent(transform, false);
		newChunk.transform.position = new  Vector3(lastChunkPos.x + chunkWidth, newChunk.transform.position.y, 0);
		UnpackChunk(newChunk);
		currentChunks.Add(newChunk);
		regionSpawns++;
		if(regionSpawns >= backgroundRegionLength){
			ChangeRegion();
		}
	}

	private void ChangeRegion(){
		regionSpawns = 0;
		currentRegion = spawnableRegions[Random.Range(0,spawnableRegions.Length)];
	}
	//If the chunk has child chunks unpacks them (This is not recursive)
	//This is to make sure children of a chunk despawn at the right time and arent destroyed by their parent
	private void UnpackChunk(BackgroundChunk parent){
		BackgroundChunk[] children = parent.GetComponentsInChildren<BackgroundChunk>();
		foreach(BackgroundChunk child in children){
			if(child != parent){ //Because unity doesnt ignore the parent in the array
				child.transform.SetParent(transform,true);
				currentChunks.Add(child);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonateButton : MonoBehaviour {

	public void OpenDonatePage()
    {
        Debug.Log("Thank you for clicking the donate button");
        Application.OpenURL("http://www.cancer.ca/en/donate/");
    }

    void OnApplicationFocus(bool focus){
        if(!focus){
            Time.timeScale = 0;
        }
        else{
            Time.timeScale = 1;
        }
    }
}

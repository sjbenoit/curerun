﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IntroText : MonoBehaviour {

	#region Member structs /Enum
	enum TextTransition{ FADE, TYPE}

	[System.Serializable]
	struct IntroTextGroup{
		public string[] TextLines;
		public TextTransition transition;
	}

	[System.Serializable]
	struct IntroScene{
		public IntroTextGroup[] scenes;
	}
	#endregion

	#region Serialized Fields
	[SerializeField]
	IntroScene[] PresetIntros;

	[SerializeField]
	Text textTemplate;

	[SerializeField]
	AudioSource introMusic;
	[SerializeField]
	UnityEvent eventsOnEnd;
	[Header("Transition Delays")]
	[SerializeField]
	float lineWaitTime = 1.75f;
	[SerializeField]
	float textFadeRate = 0.02f;
	[SerializeField]
	float textTypeRate = 0.2f;
	[SerializeField]
	float musicFadeRate = 0.01f;
	#endregion

	#region Text Positioning
	float xStart = -155f;
	float xEnd = -155f;
	float prevX = 0f;
	float yStart = 40;
	float yHeight = 40;
	#endregion

	List<Text> currentTextObjects = new List<Text>();

	Coroutine currentTextCor = null;
	

	// Use this for initialization
	void Start () {
		IntroScene introToUse = PresetIntros[Random.Range(0,PresetIntros.Length)];
		StartCoroutine(DisplayScenes(introToUse.scenes));
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	IEnumerator DisplayScenes(IntroTextGroup[] IntroScenes){
		for(int i = 0; i<IntroScenes.Length; i++){
			IntroTextGroup currentGroup = IntroScenes[i];
			prevX = xStart;
			for(int j = 0; j<currentGroup.TextLines.Length; j++){
				Vector2 position = new Vector2();
				position.x = Random.Range(prevX,xEnd);
				prevX = position.x;
				position.y = yStart -  (yHeight * j);
				currentTextCor = StartCoroutine(DisplayText(currentGroup.TextLines[j],position,currentGroup.transition));
				yield return new WaitWhile(() => currentTextCor != null);
				yield return new WaitForSeconds(lineWaitTime);
			}
			for(int j =0; j<currentTextObjects.Count; j++){
				Text currentText = currentTextObjects[j];
				currentTextCor = StartCoroutine(RemoveText(currentText,currentGroup.transition));
				yield return new WaitWhile(() => currentTextCor !=null);
			}
			for(int j = currentTextObjects.Count-1; j>=0; j--){
				Text currentText = currentTextObjects[j];
				currentTextObjects.Remove(currentText);
				Destroy(currentText.gameObject);
			}
		}
		while(introMusic.volume > 0){
			introMusic.volume -= musicFadeRate;
			yield return null;
		}
		eventsOnEnd.Invoke();
	}

	IEnumerator RemoveText(Text textToRemove, TextTransition transition){
		if(transition == TextTransition.FADE){
			for(float i =1f; i>=0f; i-= textFadeRate*2){
				Color textColour = textToRemove.color;
				textColour.a = (i<=0) ? 0 : i;
				textToRemove.color = textColour;
				yield return null;
			}
			Color finalColour = textToRemove.color;
			finalColour.a = 0;
			textToRemove.color = finalColour;
		}
		else if(transition == TextTransition.TYPE){
			for(int i = textToRemove.text.Length-1; i>=0; i--){
				textToRemove.text = textToRemove.text.Substring(0,i);
				yield return null;
			}
		}
		currentTextCor = null;
	}

	IEnumerator DisplayText(string text,Vector2 anchoredPosition,TextTransition transition){
		
		Debug.Log("Starting to display " + text);
		Text newText = Instantiate(textTemplate);
		newText.transform.SetParent(transform);
		newText.rectTransform.anchoredPosition = anchoredPosition;

		if(transition == TextTransition.FADE){
			newText.text = text;
			
			for(float i =0f; i<1f; i+= textFadeRate){
				Color textColour = newText.color;
				textColour.a = (i>=1) ? 1 : i;
				newText.color = textColour;
				yield return null;
			}
		}
		else if(transition == TextTransition.TYPE){
			newText.text = "";
			Color textColour = newText.color;
			textColour.a = 1;
			newText.color = textColour;
			for(int i =0; i<text.Length; i++){
				newText.text += text[i];
				yield return new WaitForSeconds(textTypeRate);
			}
		}

		currentTextObjects.Add(newText);
		currentTextCor = null;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flying : MonoBehaviour {

	// Used only if xEnabled is false
	[SerializeField]
	protected float xSpeed = 0f;
	// Used only if yEnabled is false
	[SerializeField]
	protected float ySpeed = 0f;
	[SerializeField]
	protected float xSpeedUp = 0f;
	[SerializeField]
	protected float xSpeedDown = 0f;
	[SerializeField]
	protected float ySpeedUp = 0f;
	[SerializeField]
	protected float ySpeedDown = 0f;
	[SerializeField]
	protected float xShift = 0f;
	[SerializeField]
	protected float yShift = 0f;
	[SerializeField]
	protected bool xEnabled = false;
	[SerializeField]
	protected bool yEnabled = false;
	[SerializeField]
	protected bool xDirection; // true is positive
	[SerializeField]
	protected bool yDirection; // true is positive
	[SerializeField]
	protected float xUpperBound = 0f;
	[SerializeField]
	protected float xLowerBound = 0f;
	[SerializeField]
	protected float yUpperBound = 0f;
	[SerializeField]
	protected float yLowerBound = 0f;

	
	// Children that inherit from this class must initialize the state of this class
	void Start () {

	}
	
	// Children that inherit from this class must delete Update() or call base.Update() or call UpdatePosition();
	void Update () {
		UpdatePosition();
		SpriteRenderer sprite = GetComponent<SpriteRenderer>();
		if(sprite != null){
			Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position + sprite.bounds.extents);
			if(screenPos.x < -20){
				Destroy(gameObject);
			}
		}
	}

	protected void UpdatePosition() {
		float xTransform = 0;
		float yTransform = 0;
		if (xEnabled) {
			xTransform = transformPosition(xSpeedUp, xSpeedDown, ref xDirection, ref xShift, xUpperBound, xLowerBound);
		} else {
			xTransform += xSpeed * Time.deltaTime;
		}
		if (yEnabled) {
			yTransform = transformPosition(ySpeedUp, ySpeedDown, ref yDirection, ref yShift, yUpperBound, yLowerBound);
		} else {
			yTransform += ySpeed * Time.deltaTime;
		}
		this.transform.position += new Vector3(xTransform, yTransform, 0);
	}

	private float transformPosition(float speedUp, float speedDown, ref bool direction, ref float shift, float upper, float lower) {
		float transform;
		if (direction) {
			transform = speedUp * Time.deltaTime;
			shift += transform;
			if (shift >= upper) {
				direction = false;
			}
		} else {
			transform = -speedDown * Time.deltaTime;
			shift += transform;
			if (shift <= lower) {
				direction = true;
			}
		}
		return transform;
	}
}


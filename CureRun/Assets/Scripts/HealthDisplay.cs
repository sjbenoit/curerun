﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

    public Player player;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    [SerializeField]
    private GameObject heartPrefab;
    [SerializeField]
    private float heartSize;
    [SerializeField]
    private float spacing;

    // Use this for initialization
    void Start () {
        int maxHealth = player.GetMaxHealth();

        Vector3 offset = new Vector3(-0.25f * (heartSize + spacing) * maxHealth, 0, 0);
        Vector3 heartOffset = new Vector3(heartSize + spacing, 0, 0);

        for (int i = 0; i < maxHealth; i++)
        {
            GameObject newHeart = Instantiate(heartPrefab, Vector3.zero, Quaternion.identity);
            Heart newHeartScript = newHeart.GetComponent<Heart>();
            newHeart.transform.SetParent(transform, false);
            newHeart.transform.position += offset + (i * heartOffset);
            newHeartScript.id = i;
            newHeartScript.fullHeart = fullHeart;
            newHeartScript.emptyHeart = emptyHeart;
            newHeartScript.player = player;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

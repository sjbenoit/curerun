﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkySpawner : MonoBehaviour {

	[SerializeField]
	GameObject[] skyPrefabs;

	float timeForSpawn = 7f;
	float timeSinceSpawn =0f;

	[SerializeField]
	float topSky, bottomSky;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceSpawn += Time.deltaTime;
		if(timeSinceSpawn > timeForSpawn){
			SpawnPrefab();
			timeSinceSpawn = 0;
		}
	}

	private void SpawnPrefab(){
		GameObject newSky = Instantiate(skyPrefabs[Random.Range(0,skyPrefabs.Length)]);
		Vector3 position = this.transform.position;
		position.y = Random.Range(bottomSky,topSky);
		newSky.transform.position = position;
	}
}

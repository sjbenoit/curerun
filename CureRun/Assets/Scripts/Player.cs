﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Balance Constants
    public float maxJumpDuration = 1f;
    public float jumpSpeed = 1.2f;
    public float fallSpeed = 0.75f;

    public int score;

    // Private. Don't mess around with.
    private float jumpDuration;
    enum JumpState { Running, Rising, Falling, Bouncing };
    private JumpState currentJumpState;

    private float bounceMultiplier = 0f;
    
    private List<Collider2D> currentGround;
    [SerializeField]
    private int groundLayer;

    [SerializeField]
    private int maxHealth = 3;
    private int currentHealth;

    [SerializeField]
    ParticleSystem runningParticles;

    // LMAO Unity animation XD
    [SerializeField]
    private Animator playerAnimator;
    int playerStateHash = Animator.StringToHash("PlayerState");

    // Use this for initialization
    void Start()
    {
        groundLayer = LayerMask.NameToLayer("Ground");
        score = 0;
        jumpDuration = 0f;
        currentJumpState = JumpState.Falling;
        runningParticles.Stop();
        runningParticles.Clear();
        currentGround = new List<Collider2D>();
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        // Animating like a hack DX
        if (currentJumpState == JumpState.Running) {
            playerAnimator.SetInteger(playerStateHash, 1);
        } else {
            playerAnimator.SetInteger(playerStateHash, 0);
        }
        playerAnimator.SetInteger(playerStateHash, (int)currentJumpState);
        if (currentJumpState == JumpState.Bouncing)
        {
            Bounce();
        }
        else
        {
            if (Input.GetMouseButton(0) && currentJumpState != JumpState.Falling)
            {
                if (currentJumpState == JumpState.Running)
                {
                    currentJumpState = JumpState.Rising;
                    runningParticles.Stop();
                    runningParticles.Clear();
                }
                if (currentJumpState == JumpState.Rising)
                {
                    Jump();
                }
            }
            else if (currentJumpState == JumpState.Falling)
            {
                Fall();
            }
            else if (currentJumpState == JumpState.Rising)
            {
                if (currentGround.Count == 0)
                {
                    currentJumpState = JumpState.Falling;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == groundLayer)
        {
            currentJumpState = JumpState.Running;
            runningParticles.Play();
            jumpDuration = 0f;

            if (!currentGround.Contains(collision))
            {
                currentGround.Add(collision);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == groundLayer)
        {
            if (currentGround.Contains(collision))
            {
                currentGround.Remove(collision);
            }
            if (currentGround.Count == 0 && !Input.GetMouseButton(0) && currentJumpState != JumpState.Bouncing)
            {
                currentJumpState = JumpState.Falling;
            }
        }
    }

    private void Jump()
    {
        jumpDuration += Time.deltaTime;
        if (jumpDuration < maxJumpDuration)
        {
            transform.position += Time.deltaTime * jumpSpeed * Vector3.up;
        }
        else
        {
            currentJumpState = JumpState.Falling;
        }
    }

    private void Fall()
    {
        transform.position -= Time.deltaTime * fallSpeed * Vector3.up;
    }

    private void Bounce()
    {
        jumpDuration += Time.deltaTime;
        if (jumpDuration < maxJumpDuration)
        {
            transform.position += Time.deltaTime * jumpSpeed * bounceMultiplier * Vector3.up;
        }
        else
        {
            bounceMultiplier = 0f;
            currentJumpState = JumpState.Falling;
        }
    }

    public void ApplyPoints(int points)
    {
        score += points;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        Debug.Log(currentHealth);
        if (currentHealth <= 0)
        {
            // DIE DIE DIE
            transform.position = Vector3.zero;
            score = 0;
            jumpDuration = 0f;
            currentJumpState = JumpState.Falling;
            runningParticles.Stop();
            runningParticles.Clear();
            currentGround = new List<Collider2D>();
            currentHealth = maxHealth;
            FindObjectOfType<MenuSystem>().ShowEndScreen(true);
            FindObjectOfType<MenuSystem>().ShowGame(false);
        }
    }

    public void ApplyEffect(string kind, int value)
    {
        if (kind == "bounce")
        {
            if (currentJumpState == JumpState.Falling)
            {
                jumpDuration = 0f;
                currentJumpState = JumpState.Bouncing;
                bounceMultiplier = value;
                runningParticles.Stop();
                runningParticles.Clear();
            }
        }
    }

    public int GetHealth()
    {
        return currentHealth;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }
}

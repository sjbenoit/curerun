﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSystem : MonoBehaviour {
	[SerializeField]
	Canvas endScreen;
	[SerializeField]
	Canvas startScreen;
	[SerializeField]
	Canvas introCanvas;
	[SerializeField]
	GameObject gameSystem;
	// Use this for initialization
	void Start () {
		endScreen.gameObject.SetActive(false);
		introCanvas.gameObject.SetActive(false);
		startScreen.gameObject.SetActive(true);
		gameSystem.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowGame(bool show){
		gameSystem.SetActive(show);
	}

	public void ShowTitle(bool show){
		startScreen.gameObject.SetActive(show);
	}

	public void ShowIntroScene(bool show){
		introCanvas.gameObject.SetActive(show);
	}
	public void ShowEndScreen(bool show){
		endScreen.gameObject.SetActive(show);
	}
}

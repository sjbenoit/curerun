﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceCounter : MonoBehaviour {

    public float speed = 1f;

    private float distance;
    private Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        distance = 0;
	}
	
	// Update is called once per frame
	void Update () {
        distance += Time.deltaTime * speed;
        text.text = "Distance: " + (int)distance + "m";
	}
}
